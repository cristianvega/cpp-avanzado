#include<cstdlib>
#include<iostream>
#include<fstream>
#include<string.h>
using namespace std;
void altas();
void bajas();
void buscar();
void consultas();
void modificaciones();
void salir();
void menu();
//Variables Globales
char nombre[30],grupo[10],resp[1];
int semestre=0, edad=0;
bool encontrado=false;
char auxGrupo[10];
int main(){
    menu();
    return 0;
    system("PAUSE");
}
void menu(){
    int opcion=0;
    do{
        cout<<"Manejo de Archivos en C++"<<endl;
        cout<<"1. Altas"<<endl;
        cout<<"2. Bajas"<<endl;
        cout<<"3. Consultas"<<endl;
        cout<<"4. Buscar un Registro"<<endl;
        cout<<"5. Modificaciones"<<endl;
        cout<<"6. Salir"<<endl;
        cout<<"Que deseas hacer? ";
        cin>>opcion;
        switch(opcion){
            case 1:
                altas();
            break;
            case 2:
                bajas();
            break;
            case 3:
                consultas();
            break;
            case 4:
                buscar();
            break;
            case 5:
                modificaciones();
            break;
            case 6:
                salir();
            break;
            default:
                cout<<"�Opcion Incorrecta!"<<endl;
        }
    }while(opcion!=6);
}//Fin funcion menu
void altas(){
    ofstream escritura;
    escritura.open("alumnos.txt",ios::out|ios::app);
    if(escritura.is_open()){
        cout<<"Ingresa el nombre del Alumno: ";
        cin>>nombre;
        cout<<"Ingresa el Semestre del Alumno: ";
        cin>>semestre;
        cout<<"Ingresa el Grupo del Alumno: ";
        cin>>grupo;
        cout<<"Ingresa la Edad del Alumno: ";
        cin>>edad;
        escritura<<nombre<<" "<<semestre<<" "<<grupo<<" "<<edad<<endl;
    }else{
        cout<<"Error, el Archivo No se Pudo Abrir"<<endl;
    }
    escritura.close();
}//Fin funcion altas

void consultas(){
    ifstream lectura;
    lectura.open("alumnos.txt",ios::out|ios::in);
    if(lectura.is_open()){
        cout<<"Registros del Archivo alumnos.txt"<<endl;
        cout<<"________________________________"<<endl;
        lectura>>nombre;
        while(!lectura.eof()){
            lectura>>semestre;
            lectura>>grupo;
            lectura>>edad;
            cout<<"Nombre: "<<nombre<<endl;
            cout<<"Semestre: "<<semestre<<endl;
            cout<<"Grupo: "<<grupo<<endl;
            cout<<"Edad: "<<edad<<endl;
            lectura>>nombre;
            cout<<"________________________________"<<endl;
        }
    }else{
        cout<<"Error, el Archivo No se Pudo Abrir, No ha sido creado"<<endl;
    }
    lectura.close();
}//Fin funcion consultas

void bajas(){
}
void buscar(){
    ifstream lectura;//Creamos la variable de tipo lectura
    lectura.open("alumnos.txt",ios::out|ios::in);//Abrimos el archivo
    //validando la apertura del archivo
    if(lectura.is_open()){
        cout<<"Ingresa el Grupo que deseas Buscar: ";
        cin>>auxGrupo;
        lectura>>nombre;//lectura adelantada
        encontrado=false;
        while(!lectura.eof()){
            lectura>>semestre>>grupo>>edad;//leyendo los campos del registro
            //Comparar cada registro para ver si es encontrado
            if(strcmp(auxGrupo,grupo)==0){
                cout<<"______________________________"<<endl;
                cout<<"Nombre: "<<nombre<<endl;
                cout<<"Semestre: "<<semestre<<endl;
                cout<<"Grupo: "<<grupo<<endl;
                cout<<"Edad: "<<edad<<endl;
                cout<<"______________________________"<<endl;
                encontrado=true;
            }
            lectura>>nombre;//lectura adelantada
        }
        if(encontrado==false){
            cout<<"No hay registros con el Grupo "<<auxGrupo<<endl;
        }
    }else{
        cout<<"No se pudoAbrir el Archivo, aun no ha sido Creado"<<endl;
    }
    //cerrando el archivo
    lectura.close();
}
void modificaciones(){
}
void salir(){
    cout<<"Programa Finalizado"<<endl;
}
